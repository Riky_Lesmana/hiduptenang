        <div class="fullwidth-block">
          <div class="container">
            <h2 class="section-title">Latest news</h2>
            <div class="row news-list">

							<?php foreach($blog_widget as $post_widget): ?>
	              <div class="col-md-4">
	                <div class="news">
		                  <figure><img src="<?php echo base_url()."files/thumb/".$post_widget->gambar_image."/370/280/fit";?>" alt="<?php echo $post_widget->title; ?>"></figure>
		                  <div class="date"><i class="fa fa-calendar"></i> <?php echo date('d/m/Y', $post_widget->created_on); ?></div>
		                  <h2 class="entry-title"><a href="<?php echo base_url('blog/'.date('Y/m', $post_widget->created_on) .'/'.$post_widget->slug); ?>"><?php echo $post_widget->title; ?></a></h2>
	                </div>
	              </div>
							<?php endforeach ?>
            </div> <!-- .row -->
          </div> <!-- .container -->
        </div> <!-- .latest-news-section -->