
        <div class="fullwidth-block" data-bg-color="#f1f1f1">
          <div class="container">
            <h2 class="section-title">Asuransi Kami</h2>
            <small class="section-subtitle"><?php echo $subtitle; ?></small>

            <div class="row">
              <div class="col-md-12">
                <div class="row">
									<?php foreach($blog_post_by_cat as $post_widget){ ?>
	                  <div class="col-md-3">
	                  	<a href="<?php echo base_url('blog/'.date('Y/m', $post_widget->created_on) .'/'.$post_widget->slug); ?>">
		                    <div class="offer caption-bottom">
		                      <img src="<?php echo base_url()."files/thumb/".$post_widget->gambar_image."/270/270/fit";?>" alt="<?php echo $post_widget->title; ?>">
		                      <div class="caption">
		                        <h3 class="offer-title"><?php echo $post_widget->title; ?></h3>
		                      </div>
		                    </div>
	                    </a>
	                  </div>
									<?php } ?>
                </div> <!-- .row -->
              </div> <!-- .col-md-12 -->
            </div> <!-- .row -->

          </div> <!-- .container -->
        </div> <!-- .offer-section -->